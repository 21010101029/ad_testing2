package com.example.ad_testing

import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugins.googlemobileads.GoogleMobileAdsPlugin
import java.security.AccessController


class MainActivity : FlutterActivity() {
    fun configureFlutterEngine(flutterEngine: FlutterEngine?) {
        super.configureFlutterEngine(flutterEngine)

        // TODO: Register the ListTileNativeAdFactory
        GoogleMobileAdsPlugin.registerNativeAdFactory(
            flutterEngine, "listTile",
            NativeAdFactorySmall(AccessController.getContext())
        )
        GoogleMobileAdsPlugin.registerNativeAdFactory(
            flutterEngine, "listTileMedium",
            NativeAdFactoryMedium(AccessController.getContext())
        )
    }

    fun cleanUpFlutterEngine(flutterEngine: FlutterEngine?) {
        super.cleanUpFlutterEngine(flutterEngine)

        // TODO: Unregister the ListTileNativeAdFactory
        GoogleMobileAdsPlugin.unregisterNativeAdFactory(flutterEngine, "listTile")
        GoogleMobileAdsPlugin.unregisterNativeAdFactory(flutterEngine, "listTileMedium")
    }
}