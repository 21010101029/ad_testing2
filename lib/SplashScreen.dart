
import 'dart:async';

import 'package:ad_testing/home_page.dart';
import 'package:flutter/material.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';

import 'AppOpenAdManager.dart';

  class SplashScreen extends StatefulWidget {
    var isBannerAdLoaded;
    var bannerAd;
  SplashScreen({this.isBannerAdLoaded,this.bannerAd});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  AppOpenAdManager appOpenAdManager = AppOpenAdManager();

  @override
  void initState() {
    super.initState();
    appOpenAdManager.loadAd();
    Future.delayed(const Duration(milliseconds: 800)).then((value) {
      appOpenAdManager.showAdIfAvailable();
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => const HomePage(),
        ),
      );
    });
  }

  // @override
  // Widget build(BuildContext context) {
  //   return const Scaffold(
  //     body: Center(
  //       child: CircularProgressIndicator(),
  //     ),
  //   );
  // }
  @override
  Widget build(BuildContext context) {
    return widget.isBannerAdLoaded
        ? SizedBox(
      width: double.infinity,
      height: 50,
      child: AdWidget(
        ad: widget.bannerAd,
      ),
    )
        : SizedBox();
  }
}